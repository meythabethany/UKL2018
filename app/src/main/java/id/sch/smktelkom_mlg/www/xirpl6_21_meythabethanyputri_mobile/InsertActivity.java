package id.sch.smktelkom_mlg.www.xirpl6_21_meythabethanyputri_mobile;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import id.sch.smktelkom_mlg.www.xirpl6_21_meythabethanyputri_mobile.database.DatabaseHelper;
import id.sch.smktelkom_mlg.www.xirpl6_21_meythabethanyputri_mobile.koneksi.VolleySingleton;
import id.sch.smktelkom_mlg.www.xirpl6_21_meythabethanyputri_mobile.model.BarangModel;
import id.sch.smktelkom_mlg.www.xirpl6_21_meythabethanyputri_mobile.model.CategoryModel;
import id.sch.smktelkom_mlg.www.xirpl6_21_meythabethanyputri_mobile.model.TypeModel;
import id.sch.smktelkom_mlg.www.xirpl6_21_meythabethanyputri_mobile.model.UserModel;
import id.sch.smktelkom_mlg.www.xirpl6_21_meythabethanyputri_mobile.session.SessionPreference;

public class InsertActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener {

    private final String url_cat = "http://10.1.5.22:8080/categories";
    private final String url_type = "http://10.1.5.22:8080/categorytypes";
    List<CategoryModel> categoryList;
    List<TypeModel> typeList;
    ArrayList<String> category = new ArrayList<String>();
    ArrayList<String> elektronik = new ArrayList<String>();
    ArrayList<String> nonElektronik = new ArrayList<String>();
    ArrayAdapter<String> dataAdapter2;
    ArrayAdapter<String> dataAdapter1;
    DatabaseHelper db;
    UserModel user;
    Spinner spinner1, spinner2;
    EditText etNama, etSpek, etDate, etHarga, etJumlah, etRuang;
    Button save;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_insert);

        db = new DatabaseHelper(this);
        spinner1 = findViewById(R.id.spin_cat);
        spinner2 = findViewById(R.id.spin_type);
        etNama = findViewById(R.id.et_namaBarang);
        etSpek = findViewById(R.id.et_spek);
        etDate = findViewById(R.id.tanggal);
        etRuang = findViewById(R.id.et_namaRuang);
        etHarga = findViewById(R.id.et_harga);
        etJumlah = findViewById(R.id.et_satuan);
        save = findViewById(R.id.save);

        if (!SessionPreference.getInstance(this).isLoaded()) {
            LoadCat();
            LoadType();
            SessionPreference.getInstance(this).Load("Loaded");
        }
        user = SessionPreference.getInstance(this).getUser();
        spinner1.setOnItemSelectedListener(this);
        dataAdapter1 = new ArrayAdapter<String>(InsertActivity.this, android.R.layout.simple_spinner_dropdown_item, category);
        dataAdapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner1.setAdapter(dataAdapter1);

        dataFill();

        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                saveData();
            }
        });
    }


    private void saveData() {
        String kategori;
        int type = 0;
        String nama = etNama.getText().toString();
        String spek = etSpek.getText().toString();
        String date = etDate.getText().toString();
        String ruang = etRuang.getText().toString();

        String cat = spinner1.getSelectedItem().toString();
        if (cat.equals("elektronik")) {
            kategori = "A";
        } else {
            kategori = "B";
        }

        String tipe = spinner2.getSelectedItem().toString();
        switch (tipe) {
            case "laptop":
                type = 1;
                break;
            case "desktop":
                type = 2;
                break;
            case "lcd":
                type = 3;
                break;
            case "kamera":
                type = 4;
                break;
            case "elektronik lainnya":
                type = 5;
                break;
            case "meja":
                type = 6;
                break;
            case "lemari":
                type = 7;
                break;
            case "sofa":
                type = 8;
                break;
            case "kursi":
                type = 9;
                break;
            case "non elektronik lainnya":
                type = 10;
                break;
        }

        int harga = Integer.parseInt(etHarga.getText().toString());
        int satuan = Integer.parseInt(etJumlah.getText().toString());

        BarangModel barang = new BarangModel(
                nama, spek, date, ruang, kategori, type, harga, satuan
        );
        db.insertBarang(barang);

        spinner1.setSelection(0);
        spinner2.setAdapter(null);
        etNama.setText("");
        etSpek.setText("");
        etDate.setText("");
        etRuang.setText("");
        etHarga.setText("");
        etJumlah.setText("");
        int id = db.getLastBarangID();
        String inventory = user.getBranch() + date.substring(0, 3) + date.substring(4, 5) + kategori + type + "/" + id;

        BarangModel barangU = new BarangModel(
                id, nama, spek, date, ruang, kategori, type, harga, satuan, inventory
        );
        db.updateBarang(barangU);

        finish();
        startActivity(new Intent(getApplicationContext(), ListActivity.class));
    }

    private void LoadType() {
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url_type, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONArray jsonArray = new JSONArray(response);
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject obj = jsonArray.getJSONObject(i);
                        TypeModel type = new TypeModel(
                                obj.getInt("IDType"),
                                obj.getString("IDCategory"),
                                obj.getString("Type")
                        );
                        db.insertType(type);
                    }
                    //Log.d("data", db.getType().toString());
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
        VolleySingleton.getInstance(this).addToRequestQueue(stringRequest);
    }

    private void LoadCat() {
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url_cat, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONArray jsonArray = new JSONArray(response);
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject obj = jsonArray.getJSONObject(i);
                        CategoryModel cat = new CategoryModel(
                                obj.getString("IDCategory"),
                                obj.getString("Category")
                        );
                        db.insertCat(cat);
                    }
                    //Log.d("data", db.getCat().toString());
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
        VolleySingleton.getInstance(this).addToRequestQueue(stringRequest);
    }

    private void dataFill() {
        category.add("Pilih Kategori");
        elektronik.add("Pilih Barang");
        nonElektronik.add("Pilih Barang");
        categoryList = db.getCat();
        for (CategoryModel cat : categoryList) {
            category.add(cat.getCategory());
        }
        dataAdapter1.notifyDataSetChanged();
        typeList = db.getType();
        for (TypeModel type : typeList) {
            if (type.getIdCategory().equals("A")) {
                elektronik.add(type.getType());
            } else {
                nonElektronik.add(type.getType());
            }
        }
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        String selectedItem = parent.getItemAtPosition(position).toString();
        switch (selectedItem) {
            case "elektronik":
                setadapter(0);
                break;
            case "non elektronik":
                setadapter(1);
                break;
        }
    }

    private void setadapter(int a) {
        switch (a) {
            case 0:
                dataAdapter2 = new ArrayAdapter<String>(InsertActivity.this, android.R.layout.simple_spinner_dropdown_item, elektronik);
                break;
            case 1:
                dataAdapter2 = new ArrayAdapter<String>(InsertActivity.this, android.R.layout.simple_spinner_dropdown_item, nonElektronik);
                break;
        }
        dataAdapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner2.setAdapter(dataAdapter2);
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {
    }


}
