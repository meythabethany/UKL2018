package id.sch.smktelkom_mlg.www.xirpl6_21_meythabethanyputri_mobile.model;

public class CategoryModel {
    String idCategory, category;

    public CategoryModel(String id, String category) {
        this.idCategory = id;
        this.category = category;
    }

    public String getIdCategory() {
        return idCategory;
    }

    public String getCategory() {
        return category;
    }
}
