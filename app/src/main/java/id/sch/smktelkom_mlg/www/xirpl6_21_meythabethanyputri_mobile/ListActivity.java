package id.sch.smktelkom_mlg.www.xirpl6_21_meythabethanyputri_mobile;

import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.ajts.androidmads.library.SQLiteToExcel;

import java.io.File;
import java.util.List;

import id.sch.smktelkom_mlg.www.xirpl6_21_meythabethanyputri_mobile.adapter.BarangAdapter;
import id.sch.smktelkom_mlg.www.xirpl6_21_meythabethanyputri_mobile.database.DatabaseHelper;
import id.sch.smktelkom_mlg.www.xirpl6_21_meythabethanyputri_mobile.model.BarangModel;
import id.sch.smktelkom_mlg.www.xirpl6_21_meythabethanyputri_mobile.model.UserModel;
import id.sch.smktelkom_mlg.www.xirpl6_21_meythabethanyputri_mobile.session.SessionPreference;

public class ListActivity extends AppCompatActivity {

    ImageButton add;
    TextView etNama, etCabang;
    RecyclerView recyclerView;
    BarangAdapter adapter;
    DatabaseHelper db;
    List<BarangModel> barangList;
    private Button logout, delete;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        delete = findViewById(R.id.buttonDelete);
        delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                delete();
            }
        });

        findViewById(R.id.buttonCreate).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String directory_path = Environment.getExternalStorageDirectory().getPath() + "/Backup/";
                File file = new File(directory_path);
                if (!file.exists()) {
                    file.mkdirs();
                }
                SQLiteToExcel sqliteToExcel = new SQLiteToExcel(getApplicationContext(), DatabaseHelper.DATABASE_NAME, directory_path);
                sqliteToExcel.exportAllTables("users.xls", new SQLiteToExcel.ExportListener() {
                    @Override
                    public void onStart() {

                    }

                    @Override
                    public void onCompleted(String filePath) {
                        Toast.makeText(getApplicationContext(), "SUKSES", Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onError(Exception e) {

                    }
                });
            }
        });

        logout = findViewById(R.id.logout);
        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
                SessionPreference.getInstance(getApplicationContext()).logout();
            }
        });

        add = findViewById(R.id.btn_add);
        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(ListActivity.this, InsertActivity.class);
                startActivity(i);
            }
        });

        if (!SessionPreference.getInstance(this).isLoggedIn()) {
            finish();
            startActivity(new Intent(this, LoginActivity.class));
        }

        db = new DatabaseHelper(this);

        etNama = findViewById(R.id.et_nama);
        etCabang = findViewById(R.id.et_cabang);
        recyclerView = findViewById(R.id.rv_barang);

        UserModel user = SessionPreference.getInstance(this).getUser();
        etNama.setText(user.getNama());
        etCabang.setText(user.getBranch());

        barangList = db.getBarang();
        adapter = new BarangAdapter(barangList);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(adapter);
    }

    private void delete() {
        // db.deleteBarang(id);
        Toast.makeText(getApplicationContext(), "SUKSES", Toast.LENGTH_SHORT).show();

    }


}
