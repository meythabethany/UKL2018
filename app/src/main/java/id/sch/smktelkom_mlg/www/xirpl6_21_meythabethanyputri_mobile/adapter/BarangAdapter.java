package id.sch.smktelkom_mlg.www.xirpl6_21_meythabethanyputri_mobile.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import id.sch.smktelkom_mlg.www.xirpl6_21_meythabethanyputri_mobile.R;
import id.sch.smktelkom_mlg.www.xirpl6_21_meythabethanyputri_mobile.model.BarangModel;

public class BarangAdapter extends RecyclerView.Adapter<BarangAdapter.ViewHolder> {
    String kategori, type;
    List<BarangModel> barangList;

    public BarangAdapter(List<BarangModel> barangList) {
        this.barangList = barangList;
    }

    @NonNull
    @Override
    public BarangAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.barang_ui, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull BarangAdapter.ViewHolder holder, int position) {

        BarangModel barang = barangList.get(position);
        holder.namaTitle.setText(barang.getNama());
        if (barang.getIdCategory().equals("A")) {
            kategori = "Elektronik";
        } else {
            kategori = "Non-Elektronik";
        }
        switch (barang.getIdType()) {
            case 1:
                type = "Laptop";
                break;
            case 2:
                type = "Desktop";
                break;
            case 3:
                type = "LCD";
                break;
            case 4:
                type = "Kamera";
                break;
            case 5:
                type = "Elektronik Lainya";
                break;
            case 6:
                type = "Meja";
                break;
            case 7:
                type = "Lemari";
                break;
            case 8:
                type = "Sofa";
                break;
            case 9:
                type = "Kursi";
                break;
            case 10:
                type = "Non-Elektronik Lainnya";
                break;
        }
        holder.kategoriText.setText(kategori);
        holder.tanggalText.setText(barang.getTgl());
        holder.typeText.setText(type);
    }

    @Override
    public int getItemCount() {
        if (barangList != null) {
            return barangList.size();
        }
        return 0;
    }

    public void remove(int position) {
        barangList.remove(position);
        notifyItemRemoved(position);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView namaTitle, kategoriText, tanggalText, typeText;

        public ViewHolder(View itemView) {
            super(itemView);
            namaTitle = itemView.findViewById(R.id.namaTitle);
            kategoriText = itemView.findViewById(R.id.kategori);
            tanggalText = itemView.findViewById(R.id.tanggal);
            typeText = itemView.findViewById(R.id.type);
        }
    }
}

