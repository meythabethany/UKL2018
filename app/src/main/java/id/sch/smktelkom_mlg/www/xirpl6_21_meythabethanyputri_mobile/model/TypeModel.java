package id.sch.smktelkom_mlg.www.xirpl6_21_meythabethanyputri_mobile.model;

public class TypeModel {
    int idType;
    String idCategory, type;

    public TypeModel(int idType, String idCategory, String type) {
        this.idType = idType;
        this.idCategory = idCategory;
        this.type = type;
    }

    public int getIdType() {
        return idType;
    }

    public String getIdCategory() {
        return idCategory;
    }

    public String getType() {
        return type;
    }
}
