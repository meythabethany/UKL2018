package id.sch.smktelkom_mlg.www.xirpl6_21_meythabethanyputri_mobile;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

import id.sch.smktelkom_mlg.www.xirpl6_21_meythabethanyputri_mobile.database.DatabaseHelper;

public class ViewActivity extends AppCompatActivity {

    protected Cursor cursor;
    DatabaseHelper dbHelper;
    TextView tv1, tv2, tv3, tv4, tv5, tv6, tv7, tv8, tv9;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view);

        dbHelper = new DatabaseHelper(this);
        tv1 = findViewById(R.id.textView1);
        tv2 = findViewById(R.id.textView3);
        tv3 = findViewById(R.id.textView4);
        tv4 = findViewById(R.id.textView5);
        tv5 = findViewById(R.id.textView6);
        tv6 = findViewById(R.id.textView7);
        tv7 = findViewById(R.id.textView8);
        tv8 = findViewById(R.id.textView9);
        tv9 = findViewById(R.id.textView10);

        SQLiteDatabase db = dbHelper.getReadableDatabase();
        cursor = db.rawQuery("SELECT * FROM barang WHERE Name = '" +
                getIntent().getStringExtra("Name") + "'", null);
        cursor.moveToFirst();
        if (cursor.getCount() > 0) {
            cursor.moveToPosition(0);
            tv1.setText(cursor.getString(0).toString());
            tv2.setText(cursor.getString(1).toString());
            tv3.setText(cursor.getString(2).toString());
            tv4.setText(cursor.getString(3).toString());
            tv5.setText(cursor.getString(4).toString());
            tv6.setText(cursor.getString(5).toString());
            tv7.setText(cursor.getString(6).toString());
            tv8.setText(cursor.getString(7).toString());
            tv9.setText(cursor.getString(8).toString());
        }

    }


}
